An unofficial LaTeX beamer template for the University of Sydney (usyd) to give your beamer slides the university branding.

Warning: this was very quickly and terribly made. Feel free to improve it. 

Usage:
* Drag and drop the usyd-beamer folder into the folder containing your .tex file (don't rename it)
* Add \input{usyd-beamer/usyd-beamer.tex} to your preamble
* Look at the example file included for how to create the title slide


